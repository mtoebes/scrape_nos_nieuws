from helper_functions import load_data
import numpy as np

data = load_data(data_path="/home/mtoebes/data/nos_nieuws/all_data.csv")

data = data[np.bitwise_not(data["title"].duplicated(keep="first"))]

data.to_csv(
    "/home/mtoebes/data/nos_nieuws/all_data.csv",
    index=False)
