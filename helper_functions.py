# %%
import os
import re
import time
import pandas as pd
from bs4 import BeautifulSoup
from contextlib import closing
from datetime import date, timedelta, datetime
from os.path import isfile, join, splitext
from requests import get
from requests.exceptions import RequestException
from typing import Dict, List, Optional, TypeVar

# %%
"""
Set specific type hints
"""
pandas_dataframe = TypeVar('pandas.core.frame.DataFrame')
bs4_beautifulsoup = TypeVar('bs4.BeautifulSoup')
bs4_resultset = TypeVar("bs4.element.ResultSet")

# %%
def load_data_chunks(data_path: str,
                     file_names,
                     news_categories) -> pd.DataFrame:
    """
    load most recent data chunks until >=24 news articles
    for each category are loaded
    """
    chunks = []
    n_file = 0
    while True:
        # load a new chunk
        new_chunk = load_data(data_path=file_names[n_file])
        # append new chunk to list of chunks
        chunks.append(new_chunk)
        # create one big data frame
        data = pd.concat(chunks, ignore_index=True)
        # clean up of category names
        data = modify_category_names(data)
        # count number of scraped news articles in each category
        article_counter: dict = {category: (data[data["category"].str.contains(
            category)].shape[0]) for category in news_categories}
        # check if number of articles in each news category is sufficient
        if min(article_counter.values()) >= 24:
            break
        # asssert that new file can exist
        assert n_file <= len(
            file_names)-1, f"not enough data chunks to get >= 24 articles in each news category"
        # increase counter
        n_file += 1

    print(f"number of loaded data files: {n_file}")
    print(f"last file: {file_names[n_file]}")
    
    return data, article_counter

# %%
def get_list_of_files_with_saved_articles(
    data_path: str) -> list:
    """
    Get a complete and sorted list of files with saved data
    """
    # get all file names in folder with saved data
    file_names = [f for f in os.listdir(data_path) if isfile(join(data_path, f))]

    # remove file extensions
    file_names = [splitext(f)[0] for f in file_names]

    # specify pattern to check if file-name is a datetime
    pattern = '^[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}'

    # only kee files that have datetimes in name
    file_names = [f for f in file_names if re.match(pattern, f)]

    # convert and sort file-names by date to enale incremental loading
    file_names = [datetime.strptime(f, '%Y-%m-%d_%H-%M-%S') for f in file_names]
    file_names.sort(reverse=True) 

    # select file names based on number of days to look back in the past
    # file_names = [f for f in file_names if f >= datetime.now() - timedelta(days=look_back_days)]

    # complete the file names to full paths
    file_names = [f"{data_path}{i.strftime('%Y-%m-%d_%H-%M-%S')}.csv" for i in file_names]

    return file_names


# %%

def modify_category_names(
    loaded_data: pandas_dataframe) -> pandas_dataframe:
    """
    perform standard operations on category names of previously scraped
    news articles
    """
    # all letters to lower case
    loaded_data["category"] = loaded_data["category"].str.lower()
    # remove "in" at start of each category entry
    loaded_data["category"] = loaded_data["category"].str.replace(r'^in', '')
    # remove spaces and newline characters
    loaded_data["category"] = loaded_data["category"].str.strip()
    # replace & with _
    loaded_data["category"] = loaded_data["category"].str.replace(r' & ', '_')
    # one category is "Regionaal nieuws" replace space with underscore
    loaded_data["category"] = loaded_data["category"].str.replace(r"regionaal nieuws", "regionaal_nieuws")
    #  rename categories to allign with url names
    loaded_data["category"] = loaded_data["category"].str.replace(r"regionaal_nieuws", r"regio")
    loaded_data["category"] = loaded_data["category"].str.replace(r"cultuur_media", r"cultuur-en-media")
    
    return loaded_data

   
def scrape_article_page(
    category_urls: List[str],
    category_name: str,
    sleep_time: Optional[int] = 1) -> pandas_dataframe:
    """
    Scrapes an individual article page
    returns:
    - author info and article text of all category articles in a dataframe
    """
    all_texts = []
    for c, url in enumerate(category_urls):
        print(
            f"count: {c} in category '{category_name}' with url: {category_urls[c]}\n")
        # initialize page content dictionary
        page_content = {}
        # save url as join variable
        page_content["url"] = url
        soup = create_soup(url=url)
        page_content = scrape_author(soup, page_content)
        # extract all article text
        text_blocks = soup.find_all("p", class_="text_3v_J6Y0G")  # text_3v_J6Y0G
        #
        texts = []
        for text_block in text_blocks:
            texts.append(text_block.text)
        # save result in dictionary
        page_content["text"] = " ".join(texts)
        # maak list van dicts om later makkelijk en snel een df te maken
        all_texts.append(page_content)

        if sleep_time:
            time.sleep(sleep_time)

    return pd.DataFrame(all_texts)  # content


def scrape_author(
    page_soup: bs4_beautifulsoup,
    input_dict: Dict[str, Optional[str]]) -> Dict[str, Optional[str]]:
    """
    scrapes author info of article page
    """
    try:
        author = page_soup.find("span", class_="bio__name").text
    except:
        author = None
    input_dict["author"] = author

    return input_dict


def scrape_category_page(
    page: bs4_resultset,
    known_urls: set,
    base_url: str) -> pandas_dataframe:
    """
    1) checks if page hasn't already been scraped
    2) if 1) == False: scrape metadata
    returns a pandas dataframe with metadata of all non-scraped pages
    """
    rows_list = []
    for article in page:
        url = f'{base_url}{article.find("a")["href"]}'
        if url not in known_urls:
            # build dictionary with link info
            link_info = {}
            link_info["datetime"] = article.find("time")["datetime"]
            link_info["url"] = base_url + article.find("a")["href"]
            link_info["title"] = article.find("h3").text
            link_info["category"] = article.find(
                "span", class_="list-items__category").text
            # create list of dictionaries
            rows_list.append(link_info)

    return pd.DataFrame(rows_list)


def get_category_urls(url_base: str, news_category: str) -> bs4_resultset:
    """
    Gets urls of selected news category page
    """
    # construct news category url
    url = url_base + "/nieuws/" + news_category + "/"
    # get html of news category main page
    soup = create_soup(url=url)
    # raw_html = simple_get(url=url)
    # soup = BeautifulSoup(raw_html, features="html.parser")
    # extract further links to individual articles from news category page
    return soup.find_all("li", class_="space-bottom-xl")


def load_data(data_path: str) -> pd.DataFrame:
    """
    Function that loads in previously scraped nos_nieuws data
    """
    data = pd.read_csv(
        data_path,
        header=0,
        parse_dates=["datetime"])
    return data


def save_data(data_path: str, data: pd.DataFrame) -> None:
    data.to_csv(data_path, index=False)


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def create_soup(url: str) -> bs4_beautifulsoup:
    """
    simply returns a beautiful soup object for an url
    """
    raw_html = simple_get(url=url)
    return BeautifulSoup(raw_html, features="html.parser")


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can
    make it do anything.
    """
    print(e)


# %%
