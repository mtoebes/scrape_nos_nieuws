# %%
import pandas as pd
from typing import List
from datetime import date, datetime
from helper_functions import (get_category_urls, save_data,
                              scrape_category_page, scrape_article_page,
                              get_list_of_files_with_saved_articles,
                              load_data_chunks)

# %%
print(f"current date and time is: {datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}")

# %%
# set main page
nos_base: str = "https://nos.nl"

# select subcategories
news_categories: List[str] = [
    "binnenland",
    "buitenland",
    "regio",
    "politiek",
    "economie",
    "koningshuis",
    "tech",
    "cultuur-en-media",
    "opmerkelijk"]

# initialize some variables
all_content = None
all_info = None
data_path = "/data/shared/data/nos_nieuws/data_chunks/"

# %%
# get list of all data files
file_names = get_list_of_files_with_saved_articles(data_path)

recent_data, _ = load_data_chunks(data_path=data_path,
                                  file_names=file_names,
                                  news_categories=news_categories)

# create set of known urls (already scraped articles)
known_urls = set(recent_data["url"])

# %%
# loop over the different news categories
for category in news_categories:
    # check news category page for list of articles
    category_page = get_category_urls(
        url_base=nos_base,
        news_category=category)

    # for each underlying article in landing page:
    # format individual links and place in dataframe
    info = scrape_category_page(
        category_page,
        known_urls=known_urls,
        base_url=nos_base)
    
    # if there are no new pages, continue to next category
    if info.shape == (0, 0):
        print(f"No new urls found in category '{category}', continue with next category\n")
        continue

    # update dataframe with all metadata (for later join)
    all_info = pd.concat([all_info, info])

    # update list of known urls
    known_urls.update(info['url'])

    # For each not already scraped article on news category page:
    # scrape page and extract relevant contents
    category_content = scrape_article_page(
        info["url"],
        category_name=category,
        sleep_time=None)

    # update dataframe with all article content
    all_content = pd.concat([all_content, category_content])

# %%
# assert any new content was found
assert all_info is not None, f"all_info is None" 
assert all_content is not None, f"all_content is None"  

# %%
# merge info en content of all categories
new_data = pd.merge(
    all_info,
    all_content,
    how="inner",
    on="url")

# Check for absence of duplicates
duplicates = new_data[new_data["title"].duplicated(keep="first")]["title"]
assert len(duplicates) == 0

# check for absence of only empty cells in text column
assert new_data["text"].isnull().any() == False

# %%
# save to disk on day by day basis
save_data(
    data_path=f"/data/shared/data/nos_nieuws/data_chunks/{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.csv",
    data=new_data)

# %%
