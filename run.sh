#! /bin/bash

# add crontab entry
# @hourly ~/bin/scrape_nos_nieuws/run.sh >> ~/bin/scrape_nos_nieuws/scrape_nos_niews.log 2>&1

~/anaconda3/condabin/conda activate nos_nieuws

~/anaconda3/envs/nos_nieuws/bin/python ~/data-code/python/scrape_nos_nieuws/main_script.py